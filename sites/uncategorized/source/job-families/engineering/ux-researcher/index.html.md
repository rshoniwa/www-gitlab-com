---
layout: job_family_page
title: "UX Researcher"
description: "At GitLab, UX Researchers collaborate with our Product Designers, Product Managers and the rest of the community to assist in determining what features should be built, behave, and priorities."
---

At GitLab, UX Researchers collaborate with our Product Designers, Product Managers and the rest of the community to assist in determining what features should be built, how they should behave, and what the priorities should be. UX Researchers report to the UX Research Manager.

## Responsibilities

Unless otherwise specified, all UX Research roles at GitLab share the following requirements and responsibilities:

#### Requirements

* [Self-motivated and self-managing](https://about.gitlab.com/handbook/values/#efficiency), with strong organizational skills.

* Share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values.

* Passionate about mentoring others.

* Evangelize research. Share user insights within the broader organization and externally in creative ways to increase empathy.

* Empathetic, curious and open-minded.

* Able to thrive in a fully remote organization.

* Able to use GitLab.

#### Nice to haves

* Enterprise software company experience.

* Developer platform/tool industry experience.

* A degree in psychology, human factors, human-computer interaction, or a related field.

### UX Researcher

#### Job Grade

The UX Researcher is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Personally initiate and conduct research within or across your stage groups to build deep foundational knowledge of your coverage areas. Research should consist of a mix of solution validation and problem validation projects.

* Communicate, advocate for, and socialize actionable insights in a clear and compelling way.

* Know your stage groups: understand the technology, experiences, and features of your assigned stage groups. Maintain a thorough knowledge of the direction and vision for your assigned stage groups.

* Know your stage group users: develop a deep empathic understanding of your users, learn about their needs, and identify gaps.

* With the help of your manager and product leadership, effectively prioritize your research efforts across stage groups. Align with Product teams while considering impact, the goals of your stage groups, the broader product vision, and company objectives.

* Respond to solution validation user research inquiries that are escalated by Product Design.

* Guide Product Managers and Product Designers to conduct research with a high degree of rigor.  Ensure they are following defined research processes (ex: handbook procedures, opening and closing issues for research studies, documenting actionable insights, etc).

* Actively contribute to the UX Research team culture, processes, and documentation.

### Senior UX Researcher

#### Job Grade

The Senior UX Researcher is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Personally initiate and conduct research projects within your assigned stage groups and across adjacent stage groups to build deep foundational knowledge of your areas of coverage. Research should consist of a mix of problem validation, foundational research, and strategic research projects.

* Drive a company-wide research-based measure: design or improve upon a methodology (if needed), collect and report out findings.

* Demonstrate thought leadership by delivering 2 [strategic research](/handbook/engineering/ux/ux-research-training/strategic-research-at-gitlab/) projects per year.

* Communicate, advocate for, and socialize actionable insights in a clear and compelling way

* Know your stage groups: understand the technology, experiences, and features of your assigned and adjacent stage groups. Maintain a thorough knowledge of the direction and vision for your assigned stages and stage groups.

* Know your stage group users: develop a deep empathic understanding of your users, learn about their needs, and identify gaps.

* Business acumen: consider the team's business goals when approaching research projects to ensure there's alignment.

* Effectively prioritize your research efforts across stage groups. Align with Product teams while considering impact, the goals of your stage groups, the broader product vision, and company objectives.

* Respond to solution validation user research inquiries that are escalated by Product Design.

* Guide Product Managers and Product Designers to conduct research with a high degree of rigor.  Ensure they are following defined research processes (ex: handbook procedures, opening and closing issues for research studies, documenting actionable insights, etc).

* Demonstrate, explain, and teach others how to apply a range of research methods, understanding the balance and trade offs between scrappy research and rigor.

* Actively contribute to the UX Research team culture, processes, and documentation.

* Lead and mentor other Researchers.

### Staff UX Researcher

#### Job Grade

The Staff UX Researcher is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Through your research, build a knowledge base that positions yourself as a thought leader within GitLab.

* Personally initiate and conduct research within your assigned stage groups, across adjacent stage groups, and across GitLab, to build deep foundational knowledge of GitLab. Research should consist of a mix of problem validation, foundational research, and strategic research projects. When possible, include other Researchers to participate in these projects.

* Know GitLab: understand the technology, experiences, and features of the GitLab product. Maintain a thorough knowledge of the direction and vision for your assigned and adjacent stage groups.

* Know GitLab users: develop a deep empathic understanding of GitLab users in various contexts, learn about their needs, and identify gaps.

* Business acumen: consider the team's business goals when approaching research projects to ensure there's alignment.

* Effectively prioritize your overall research efforts. Align with Product teams while considering impact, type of research, the goals of your stage groups, the broader product vision, and company objectives.

* Develop and maintain the GitLab strategic research program. Educate and mentor others on strategic research while demonstrating the value of strategic research.

* Demonstrate thought leadership by delivering six [strategic research](/handbook/engineering/ux/ux-research-training/strategic-research-at-gitlab/) projects per year to showcase.

* Deliver impactful research that informs decisions across our product lifecycle.

* Communicate, advocate for, and socialize actionable insights in a clear and compelling way

* Guide Product Managers and Product Designers to conduct research with a high degree of rigor.  Ensure they are following defined research processes (ex: handbook procedures, opening and closing issues for research studies, documenting actionable insights, etc).

* Demonstrate, explain, and teach others how to apply a range of research methods, understanding the balance and trade-offs between scrappy research and rigor.

* Respond to solution validation user research inquiries that are escalated by Product Design.

* Identify research training needs for the UX and Product Department. 

* Actively contribute to the UX Research team culture, processes, and documentation.

* Actively seek out difficult impediments to our efficiency as a team (process, tooling, etc), and propose and implement solutions that will enable the entire team to work more efficiently.

* Lead and mentor other Researchers.

### Principal UX Researcher

#### Job Grade

The Staff UX Researcher is a [grade 9.5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Through your research, build a knowledge base that positions yourself as a thought leader and subject matter expert within GitLab.

- Personally initiate and conduct meaningful research that spans across sections vs. stage groups, to build foundational knowledge of GitLab. Research should consist of a mix of problem validation, foundational research, and strategic research projects. When possible, include other Researchers to participate in these projects.

- Know GitLab: understand the technology, experiences, and features of the GitLab product. Maintain a thorough knowledge of the direction and vision at the stage and company level.

- Know GitLab users: develop a deep empathic understanding of GitLab users in various contexts, learn about their needs, and identify gaps. Emphasis on GitLab users who use multiple sections.

- Business acumen: consider business goals when approaching research projects to ensure there's alignment and maximum applicable insights.

- Effectively prioritize your overall research efforts. Align with section Directors, VPs, and C-level executives while considering impact, type of research, the broader product vision, and company objectives.

- Demonstrate thought leadership by delivering six strategic research projects per year to showcase.

- Deliver impactful research that informs decisions across our product lifecycle.

- Communicate, advocate for, and socialize actionable insights in a clear and compelling way.

- Demonstrate, explain, and teach others how to apply a range of research methods, understanding the balance and trade-offs between scrappy research and rigor.

- Respond to solution validation user research inquiries that are escalated by Product Design.

- Identify research training needs for the UX and Product Departments.

- Actively contribute to the UX Research team culture, processes, and documentation.

- Actively seek out difficult impediments to our efficiency as a team (process, tooling, etc), and propose and implement solutions that will enable the entire team to work more efficiently.

- Lead and mentor other Researchers.

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.

## Performance indicators

* [Perception of System Usability](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability)

* [Proactive vs Reactive UX Work](/handbook/engineering/ux/performance-indicators/#ratio-of-proactive-vs-reactive-ux-work)

## Relevant links

- [UX Research Handbook](/handbook/engineering/ux/ux-research/)

- [UX Department Handbook](/handbook/engineering/ux/)

- [Product Handbook](/handbook/product/)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

* Selected candidates will be invited to schedule a 1-hour [screening call](https://about.gitlab.com/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters or the Hiring Manager. In this call we will discuss your experience, understand what you are looking for in a UX Research role, discuss your compensation expectations, reasons why you want to join GitLab and answer any questions you have. We will also cover some scenario-based questions where will look to understand how you handle common situations that UX Researchers find.  

* Next, we'll invite you to record a case study walkthrough video. The recording is expected to be **20- minutes in duration** and returned **within ten days** of the screening call. In addition to the details below, [this discussion on our GitLab Unfiltered YouTube playlist](https://youtu.be/VU2BKjGoer4) provides an overview of what we're looking for.  

> ### Why we ask for a pre-recorded presentation
> 
> 1. It helps create a shared understanding of your work between you and our interviewers ahead of the first interview. 
> 1. It allows us to ask more informed questions about your experience and work during the interview process. 
> 1. Generally, we [avoid using meetings as a means to present](https://about.gitlab.com/handbook/communication/#common-meeting-problems). Instead, we record presentations, upload to our Unfiltered Youtube channel, and use a meeting as a Q&A.  > This interview exercise provides you with an opportunity to experience this style of work.  
> 
> ### Presentation Topics
> 
> 1. An introduction; who you are, where you're based, you're background, and why you're a UX Researcher.
> 1. An overview of one *or* two research studies. We want to understand; the size, discipline make-up, and structure of the team you were a part of; the goals of the project; how you approached research;  what you personally did on the project, how you synthesized research data to inform product and design decisions; the final output of your research; the challenges you faced throughout the project; and the meaningful business impact that the research resulted in. 
>
> ### What we're looking for 
> 
> 1. A recording that is 20 minutes in duration and returned to us within ten days of your screening call being completed. 
> 1. Ideally, one study you present should be relevant to the work you'd expect do at GitLab. This could be because the study involved research into the [persona(s)](/handbook/marketing/strategic-marketing/roles-personas/#user-personas) you'd study at GitLab, the study related to the DevOps or Developer tooling industry, the study relates to the [Stage](/handbook/product/categories/#devops-stages) you're interviewing for, is a study that went across multiple methods, or was a study that would be considered complex in nature. 
> 1. You share the recording using either a [private Youtube link](https://support.google.com/youtube/answer/157177?co=GENIE.Platform%3DDesktop&hl=en), a [Google Drive link](https://support.google.com/drive/answer/2494822?co=GENIE.Platform%3DDesktop&hl=en), or a [Loom link](https://www.loom.com/). 
> 1. Your presentation to address each of the topics listed above.  
> 
> ## Some useful resources
> 
> 1. Unsure on where to get started? Check out our [handbook page on recording a presentation in Zoom](https://about.gitlab.com/handbook/tools-and-tips/zoom/#how-to-share-a-presentation-in-zoom). A free Zoom account is all you'd need to complete this presentation. 
> 1. Interested in the type of work our UX team does? Check out our [UX Showcases on GitLab Unfiltered](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz), our [UX Research handbook](https://about.gitlab.com/handbook/engineering/ux/ux-research/), and our [UX Department's handbook](/handbook/engineering/ux/).
> 1. We work with a [low level of shame](/handbook/values/#low-level-of-shame) at GitLab. In this context, this means we don't expect your presentation to be highly polished.
> 1. Other questions? Would like the Recruiter to review your presentation before the review from the Hiring Manager? Don't hesitate to get in touch with the Recruiter who conducted your screening call. 

* We will schedule the next stages if the recording indicates your past experiences will lead to your success in a role at GitLab.

* Candidates will be invited to a 1-hour interview with the Director of UX Research. You will discuss your career, your experiences as a UX Researcher, and the role of UX Research at GitLab. 

* The next interview is 50 minutes with a UX Researcher. Overall, this interview aims to understand how you choose your research methods, how you present results, how you work with a wider team of counterparts, and how you determine whether your research resulted in a meaningful business impact. This interview will also look to understand the softer skills you have as a researcher and how you apply those in the real world. 

* Candidates will then be invited to a 50-minute interview with either a Product Design Manager or Product Designer. In this interview, we'll focus on the end result of a research study you've outlined in your case study presentation. We want to understand the question(s) you were addressing (and how you determined these questions), your methodology, and how you shared that information with the wider team. This interview be an opportunity for you to understand the stage you will a part of in more detail, hear a different perspective on the stable counterpart relationship, and ask any questions you have. 

* The final interview is 50 minutes with a Product Manager. The scheduling of this interview is contingent on positive feedback in the previous interviews. This interview be an opportunity for you to understand the stage you will a part of in more detail, hear a different perspective on the stable counterpart relationship, and ask any final questions you have. 

* Successful candidates will subsequently be made an offer via Zoom.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).

[groups]: /company/team/structure/#groups
