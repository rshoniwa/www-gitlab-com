---
layout: handbook-page-toc
title: "GitLab Technology Partnerships"
description: GitLab is open to collaboration and committed to building technology partnerships in the DevOps ecosystem. Through product integrations, GitLab helps developers compile all their work into one tool that can be accessed anywhere. We work closely through partnerships to provide developers with a single DevOps experience.
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

GitLab is experiencing tremendous growth, which creates opportunities for our [Technology Partners](https://about.gitlab.com/partners/technology-partners/). We created the Technology Partner track within the GitLab Partner Program to support partners that are looking to integrate with our solution. Through product integrations, GitLab helps developers compile all their work into one tool that can be accessed anywhere. We work closely through partnerships to provide developers with a single DevOps experience. 

We currently support integrations through our APIs or [direct additions to our product](https://about.gitlab.com/handbook/product/product-principles/#avoid-plugins-and-commercial-marketplaces), and we encourage integration partners to make and maintain the integrations with us. The work required to deliver the integration will be provided by the partner. 

Although we welcome all partners, our program is structured to provide additional benefits to technology partners that have strong integrations driven by customer demand and are making investments in a deeper GitLab relationship. Read more about the requirements and steps to become a Technology Partner below.

## Are you an individual contributor to GitLab wanting to share your work with the open source community?
Join our community of 3,000+ contributors. To get started, visit our [GitLab Community page](https://about.gitlab.com/community/) and learn more about resources, programs, and events.

## Steps to become a Technology Partner

There are two steps to become a GitLab Technology Partner and to be listed on our Technology Partners page, these include:


#### 📌 Step 1: Register on Partner Portal and Create New Partner Issue

1. Complete registration on the [Partner Portal](https://partners.gitlab.com/).
    - Make sure to select "Technology/Software/Platform" under Partner Applicant Type and then select "Technology Integration" under Partner-Type.
2.  Fill out the [New Partner Issue](https://gitlab.com/gitlab-com/alliances/alliances/issues/new?issuable_template=new_partner) using the “New Partner” template. 
3. If you are a partner who wants to integrate into our [Secure](https://about.gitlab.com/direction/secure/) and/or Protect stages, please visit the [Secure Partner Integration - Onboarding Process page](https://docs.gitlab.com/ee/development/integrations/secure_partner_integration.html) for more information. 
4. For all other integrations across the rest of the GitLab stages, complete the integration via [API](https://docs.gitlab.com/ee/api/), [webhook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) or [CI templates](https://docs.gitlab.com/ee/development/cicd/templates.html#development-guide-for-gitlab-cicd-templates). 
4. If you have questions about the integration work, sign up for [Technology Partner Office Hours](https://calendar.google.com/calendar/selfsched?sstoken=UUtGOTlrbVNIbHVjfGRlZmF1bHR8MzU5ZWY1MzY2NzAxNmU5YmYxODZlYWM3YWU5ODZjNzQ). Office Hours take place bi-weekly on Monday's from 12:00 pm - 1:00 pm Pacific Time.

>**What if I am having an issue with building my integration?**
We're always here to help you through your efforts of integration. If there's a missing API call from our current API, or you ran into other difficulties in your development please feel free to create a new issue on the [GitLab issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/) and ping [@gitlab-org/ecosystem-team](https://gitlab.com/gitlab-org/ecosystem-team).


>**What else can I do while I am building my integration?**
Once you have applied to become a partner, you will have access to the GitLab Partner Portal and other members of your team will be able to register and be associated with your account. 


The Alliances team manages new partner requests and will review the information. They will reach out to you with an update on the status of the application or a request for additional information. If you have any questions about the status of your request, please reach out to the [Alliances team](mailto:Alliance@gitlab.com).



#### 📌 Step 2: Create tech docs, messaging, identify customers, etc.

Once you have completed Step 1, you can now work on Step 2!

1. Make technical documentation on the integration publicly available on your website.
2. Create messaging that focuses on the value of the joint integration.
3. Identify mutual customer(s).
4. Add [GitLab logo](https://about.gitlab.com/press/press-kit/#logos) on your website following these [guidelines](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-guidelines/#brand-guidelines).
5. Add your Company logo and documentation to be listed and promoted on the [GitLab Technology Partners page](https://about.gitlab.com/partners/technology-partners/). Follow these [instructions on how to get your app listed](https://about.gitlab.com/handbook/alliances/integration-instructions/).

Once you have created the Merge Request, the Alliances team will be notified and will review the information. If the requirements are met and the listing is ready for approval, the Alliances team will approve the MR to get your app listed on our website. 


#### 📌 Step 3: Marketing Support

All new Technology Partners will receive the following marketing support: 

*   **Technology Partner Badge:** Once Step 2 is completed, you will receive your official GitLab Technology Partner badge to put on your website.
*   **Partner Listing:** You will be listed on [GitLab’s Partner Page](https://about.gitlab.com/partners/technology-partners/).
*   **Partner Press Release (Announcement for Integration):** We have a standard announcement [press release template](https://drive.google.com/file/d/1XvGWSqo6uOVZTR1Co4Af7PSNSLsGTOWv/view?usp=sharing) that you can use and request a GitLab quote, please contact your Partner Manager for more details once the press release is drafted
*   **Let’s Get Social**: If you create social posts, let us know so we can retweet and share to help amplify our joint integration and value.
*   **Use of GitLab Brand Assets** following these [guidelines](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-guidelines/).


### 📌 Step 4: Deal Registration Program for Technology Partners 

Congratulations! 🙌 As a GitLab Technology Partner, you are now officially able to register deals with GitLab in our [Partner Portal](https://partners.gitlab.com/). Partner Initiated Opportunities (PIOs) must be a new opportunity to our sales team and can be for a new or existing customer. The partner is also expected to assist the GitLab Sales team in closing the sale. 

To learn more, visit the [Deal Registration Program overview](https://about.gitlab.com/handbook/resellers/#the-deal-registration-program-overview) and the [Deal Registration Instructions](https://about.gitlab.com/handbook/resellers/#deal-registration-instructions) for additional details. 

### Once 3 PIOs (Partner Incremental Opportunities) are logged and closed, we may offer additional potential go-to-market support (subject to review and approval by GitLab Partner Marketing). Examples include:


*   **Joint solution collateral:** Such as datasheets, solution briefs, or tech whitepapers created by the partner will be reviewed and edited by partner marketing*   
*   **Potential co-sponsoring opportunities:** These co-sponsorship options can be both paid and unpaid types.
*   **Partner, 3rd party hosted webinar:** Provided certain guidelines are met for each option, the GitLab partner marketing team will evaluate the request on a case by case basis and provide next step guidance.
*   The goal here is ensure that we have the customers voice involved in the marketing and shaping of a partnership.

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Additional Support 


#### Contact Us 

We are here to help. The Alliance team works from issues and issue boards. If you are needing our assistance with any project, please [open an issue](http://gitlab.com/gitlab-com/alliances/alliances/issues/new) and we’ll get back to you as soon as we can! When creating an issue, please select _New_Partner_ issue template in the drop down. If it’s technical assistance you’re looking for, please see below for troubleshooting.


#### Community Engagement

We also encourage our partners to participate in the GitLab community, for example: [contributing](https://about.gitlab.com/community/contribute/) to GitLab FOSS, hosting a [GitLab Virtual Meetup](https://about.gitlab.com/community/virtual-meetups/), participating in [GitLab Heroes](https://about.gitlab.com/community/heroes/), or engaging the community in other ways. Partners are welcome to bring questions or ideas around growing our communities directly to our Community Relations team via [evangelists@gitlab.com](mailto:evangelists@gitlab.com).  


#### Dedicated Project under Alliance Group

If you’re looking for a home or an entrypoint for your joint solution on Gitlab.com, you can request a GitLab subgroup within our Alliance group [here](https://gitlab.com/gitlab-com/alliances). Please submit an issue [here](https://gitlab.com/gitlab-com/alliances/alliances/issues/new?issuable_template=new_sub-group_request) using the template _New Subgroup Request_. The partner subgroup will be created as private until the prerequisites are filled out. See issue template for more details. 


#### GitLab.com Subscription Sandbox Request

GitLab.com and GitLab Ultimate share the same core code base. If you’re looking to quickly test and integrate with GitLab, often a project on GitLab.com can be the quickest way to get started. We’re happy to provision you a private sandbox subgroup in our [Alliances GitLab.com Group](https://Gitlab.com/gitlab-com/alliances) where you can create projects for demo, R&D, and testing purposes. To make the project/sandbox public to share with external parties outside of GitLab and Partner, we request you first complete the ReadMe.md file in your Public Project Repository. [Here](https://gitlab.com/gitlab-com/alliances/google/public-tracker) is an example. Also, it’s highly recommended to maintain a demo project as well for interested external parties.


#### Requesting Ultimate Dev License for R&D 

We are able to issue Ultimate licenses for partners that are developing and testing their integration with GitLab. These licenses are only open to those working on a GitLab Ultimate specific integration. Licenses will be issued for 6 months and for up to 10 users upon request. Please reach out to your Partner Manager or add it as a comment in the following [issue template](https://gitlab.com/gitlab-com/alliances/technology-partners/issues/new). 
