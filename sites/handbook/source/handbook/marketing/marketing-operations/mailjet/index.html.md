---
layout: handbook-page-toc
title: "MailJet"
description: "MailJet is used for large scale transactional email sends"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}


### About MaailJet
[MailJet](https://www.mailjet.com/) is an email platform used for large transactional sends. It is replacing MailChimp as our large-scale sender. MailJet charges based on volume sent, not database size, so it is more cost effective (among other reasons) to use vs [Marketo](/handbook/marketing/marketing-operations/marketo/).

MailJet is used by only certain Marketing Ops and Marketing Campaigns users. It is currently being onboarded as of 2021-03-01, planning to have the IP warmed and the platform useable by mid-April 2021.

### Use Cases
- Large sends (50k) or more.
     - Security Incidents
     - Account Updates (Account and pricing notifications, etc)
     - Privacy Policy Updates

### Requesting Email Send from MailJet
If the email is an emergency, please follow directions on [this page](/handbook/marketing/emergency-response).

If the email is a non-emergency, please follow directions on [this page](/handbook/marketing/demand-generation/campaigns/emails-nurture/#overview).


## Using MailJet
Only a select number of marketing and marketing operations team members have access to send via MailJet.


### MailJet Templates
Depending on the send, there are different templates that should be used. Templates are a work in progress, follow status updates in this [epic](https://gitlab.com/groups/gitlab-com/-/epics/1249).

### MailJet Lists
WIP
