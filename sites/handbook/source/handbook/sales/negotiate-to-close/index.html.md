---
layout: handbook-page-toc
title: "Negotiate to Close"
description: "A sales negotiation is a strategic discussion (or series of discussions) between buyer and seller that ideally leads to a deal being closed. The main goal of the negotiation process is to reach an agreement that's acceptable to everyone."
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
A sales negotiation is a strategic discussion (or series of discussions) between buyer and seller that ideally leads to a deal being closed. The main goal of the negotiation process is to reach an agreement that's acceptable to everyone. Successful negotiation outcomes often start with the sales professional having and demonstrating the right mindset--one focused on a building a long-term partnership whereby both parties (buyer and seller) benefit more by working together than if the partnership did not exist. 

In his book ["Closing Time"](https://www.amazon.com/Closing-immutable-Sales-Negotiation-Hubsher/dp/0981789005), Ron Hubsher highlights the 7 immutable laws of sales negotiation:
1. You must be the buyer's 1st choice
1. Know the financial benefit your solution creates
1. Anticipate getting squeezed on price
1. Be proactive on budget and remove decision-making obstacles in advance
1. Expand the pie with a set of non-monetary trade-ups
1. Never give without getting
1. Know your walk-away price and conditions

## When Do Negotiations Start
In addition to demonstrating the right mindset, successful negotiation outcomes begin with embracing a value-based sales methodology like [Command of the Message](/handbook/sales/command-of-the-message/) from the very onset of the sales process. In fact, the below critical sales skills are prerequisites for a successful negotiation:

### Uncover Customer Needs
Conduct effective, ongoing discovery to
- Obtain a deep understanding of the customer's current state (see [Sales Discovery and Qualification Questions](/handbook/sales/qualification-questions/))
- Identify the Positive Business Outcomes they want or need to achieve (and why), and 
- Co-create a shared vision for the Required Capabilities to get from the current state to the desired future state

### Articulate Value and Differentiation
Don't start a negotiation is you are not the buyer's 1st choice. With a thorough understanding of your customer's needs, be sure to:
- Effectively articulate how GitLab will address the customer's requirements (leverage [these core resources](/handbook/sales/command-of-the-message/#resources-core-content))
- Successfully connect the dots between technical metrics and the business value that GitLab delivers (see [Demystifying the Metrics Conversation](https://about.gitlab.com/handbook/sales/command-of-the-message/metrics/)), and 
- Share relevant [proof points](/handbook/sales/command-of-the-message/proof-points/) that highlight how and why GitLab can deliver against the customer's requirements better than any other option(s) available

## Closing
The [Command Plan](/handbook/sales/command-of-the-message/command-plan/) provides a framework to maximize your likelihood of winning and securing the customer's business for a defined purchase initiative. Before even attempting to move to Close, be sure you have compelling answers to every component of the [Opportunity Overview](/handbook/sales/command-of-the-message/command-plan/#opportunity-overview) section of the Command Plan.

While there are [numerous closing techniques](https://spotio.com/blog/sales-closing-techniques/) out there, the LinkedIn Learning course referenced below recommends four common closing techniques (these need not be mutually exclusive):
1. **The Summary Close** - reiterating all the key benefits of your offer as they relate to the customer's prioritized requirements before you ask for the order
1. **The Balance Sheet Close** - collaboratively making a list of pros and cons to help the customer feel more comfortable with the decision to move forward (note: be sure you are confident that the pros significantly outweigh the cons!)
1. **The Needs Close** - methodically go through the list of all of the customer's requirements to explicitly show how you are addressing their needs and will help them achieve their desired results
1. **The Assumptive Close** - gently nudging the customer to move forward in a non-manipulative way when it seems clear that your solution meets all of the customer's requirements (most effective when you have an established and trusted relationship with the buyer)

## Recommended Resources
- ["Closing Time"](https://www.amazon.com/Closing-immutable-Sales-Negotiation-Hubsher/dp/0981789005) by Ron Hubsher
- LinkedIn Learning course: [Sales: Closing Strategies](https://www.linkedin.com/learning/sales-closing-strategies/) (38 minutes)
- LinkedIn Learning course: [Sales Negotiation](https://www.linkedin.com/learning/sales-negotiation/) (58 minutes)
- [Finding Success With Procurement](https://audiblereadypodcast.libsyn.com/34-finding-success-with-procurement-w-tim-caito) (podcast, 22 minutes)
