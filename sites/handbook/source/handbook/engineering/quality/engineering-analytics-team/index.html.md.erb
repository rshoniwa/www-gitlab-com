---
layout: handbook-page-toc
title: "Engineering Analytics Team"
description: "Engineering Analytics Team"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

- - -

## Mission

The goal of the team is to build and improve on the data capabilities needed to support a highly productive Engineering Division at GitLab. This team leverages our data analytics staffing and applies their efforts to the highest needs of Engineering.


## Vision

The engineering analytics team will enhance the metric capababilities of the engineering department by focusing on end user needs, cross-department collaboration, and industry best practices. 

Some things we will focus on include:
* Work with stable counterparts to provide actionable insights to yield better business outcomes
* Provide efficient data analytics and dashboarding capabilities on top of GitLab’s Data team warehouse infrastructure.
* Collaborating across departments to find common themes and find the best ways to represent similar metrics
* Propose new data and measurements to provide additional clarity to existing measurements.
* Analyze trends and propose strategic improvements to Engineering Leaders.
* Own creation, optimization and maintenance of the Engineering Division's and its departments' KPIs and PIs.
* Drive the creation, standardization, and implementation of all Engineering performance indicators.


## Strategy

Our plan to enable Engineering excellence in analytics is to:
* Focus on providing metrics that show a story that the users can act upon
* Narrow performance indicators to focus on most important metrics that track towards major themes of each department
* Support more efficient department key reviews with handbook driven performance indicators 
* Support stable counterpart departments with more granular & specific analysis that track towards day to day performance of the department towards its overall mission

## Team Responsibilities
The team works on the following items ordered by usual importance
* Support stable counterpart department with KPI/PI & other analysis
* Enable effective Engineering Department Key Reviews
* Support wider Eng Department with analysis & new metric requests
* Improve our handbook pages iteratively, ensure that it’s up to date


## Team Structure

This new team will be a new function under the Quality Department operating as a team of Data Analysts, led by an Engineering Manager reporting to the Quality Department Leader.

```mermaid
graph TD
    A[Quality Department]
    A --> B[Engineering Analytics Team]
    B --> BA[Data Analyst]
    B --> BB[Data Analyst]
    A --> CA(Quality Engineering teams)
    CA --> CAA[Software Engineer in Test]
    CA --> CAB[Software Engineer in Test]
    CA --> CAC[Software Engineer in Test]
    A --> D[Engineering Productivity Team]
    D --> DA[Engineering Productivity Engineer]
    D --> DB[Engineering Productivity Engineer]
    style B fill:#bbf,stroke:#f66,stroke-width:2px, color:#fff
    style BA fill:#bbf,stroke:#f66,stroke-width:2px, color:#fff
    style BB fill:#bbf,stroke:#f66,stroke-width:2px, color:#fff
```

## How we Work

While this team operates as a single team reporting to one manager, we emphasize on ensuring the prioritization and needs of Engineering Leaders via stable counterparts.

### Counterpart Assignments

The team structure will leverage [stable counterpart](https://about.gitlab.com/handbook/leadership/#stable-counterparts) assignees to ensure proper allocation and attention needed to service all Engineering Departments and Leaders and still promote cross-functional collaboration and ownership of an area.

We assign the stable counterpart by Engineering Division’s sub-departments. This is identified by the Engineering Department name assigned to an Analyst title. E.g. `Senior Data Analyst, Development`

| Eng Department | Analyst                                    | PI Page                                                                                              |
|----------------|--------------------------------------------|------------------------------------------------------------------------------------------------------|
| Engineering Org    | Team           |      [Eng PI Page](https://about.gitlab.com/handbook/engineering/performance-indicators) |
| Development    | [Lily](https://gitlab.com/lmai1)           |      [Dev PI Page](https://about.gitlab.com/handbook/engineering/development/performance-indicators) |
| Infrastructure | [Davis](https://gitlab.com/davis_townsend) | [Infra PI Page](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators) |
| Quality        | Team                                       |      [Quality PI Page](https://about.gitlab.com/handbook/engineering/quality/performance-indicators) |
| Security       | Team                                       |    [Security PI Page](https://about.gitlab.com/handbook/engineering/security/performance-indicators) |
| Support        | Team                                       |                  [Support PI Page](https://about.gitlab.com/handbook/support/performance-indicators) |
| UX             | Team                                       |               [UX PI Page](https://about.gitlab.com/handbook/engineering/ux/performance-indicators/) |

Data analysts assigned to one area are experts in that area and may not have the knowledge depth in other areas. As such contributing cross domain expertise will only be limited to Sisense charting and not beyond this data layer.

### Meetings and Scheduled Calls

Aside from the below listed scheduled meetings, the team will also have scheduled 1:1s with other team members and their stable counterpart assignement's department head at a cadence of their choosing to discuss any topics that were not covered in normal status updates or other meetings.

#### Monthly/Bi-Monthly Key Reviews

Each Engineering department has a monthly or bi-monthly key review where e-group goes over the department's metrics and can ask the department heads any general questions regarding the department's progress towards its goals. Analysts are expected to go to their stable counterpart's key meetign & participate if needed, & are encouraged to attend other department key reviews as time permits.
* If stable counterpart department head has delegated you as DRI for a particular metric, provide updates during meeting for those metrics.
* Support department heads in metric preparation ahead of reviews
* Capture follow-up asks related to metrics from key review

#### Weekly sync with stable counterparts

The Data Analysts will hold weekly meetings with all Engineering department heads to:
* Review work prioritization
* Capture strategic asks
* Ensure completion of follow up for Key Reviews

#### Bi-Weekly review with GitLab’s Data Team

This team will have standing bi-weekly sync with the Data team to:
* Drive data dependencies to resolution
* Propose initiatives to improve our data accuracy and hygiene

### Issue Boards
* [Eng-Metrics Board](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/1942495?label_name[]=Engineering%20Metrics)

### Issue Weighting

These weights are used to estimate analyst time needed to fulfill a request by 1 engineering analyst. This is used when estimating total capacity of work that can be done by the team. If more than 2 weeks needed (>8 weight), then issue needs to be broken down into multiple issues.

| Label Weight | 1 Analyst Time Estimate | Work Example |
| ----------- | ----------- |
| 1 | 1 full day | basic handbook updates, changing basic parameters in Sisense charts|
| 3 | 1/2 week | Creating new Performance Indicator from Data that already exists in data warehouse|
| 5 | 1 week | Creating new dashboard with granular details around existing metric|
| 8 | 2 weeks | introducing new metric based from data source that doesn't currently exist in data warehouse|


### Labels

#### Workflow labels
The Engineering Analytics team uses scoped workflow labels to track different stages of issues

| Label State | Description |
| ----------- | ----------- |
| ![Triaging](img/label-triaging.png) | Issue has been ackowledged and we are working through scoping how much work is required to complete the request |
| ![Scoped](img/label-scoped.png) | Request has been clarified & amount of work required to fulfill request has been identified.|
| ![In Progress](img/label-in-progress.png) | Issue is actively being worked on |
| ![Waiting](img/label-waiting.png) | Team is waiting for dependency to complete work to continue progress, or work has been de-prioritized|
| ![In Review](img/label-in-review.png) | Issue has been completed & work is under review by stakeholder|

### How to Engage with Us

Feel free to reach out to us by opening an issue on [Eng-Metric Issue Board](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/1942495?label_name[]=Engineering%20Metrics) or contacting us in one of the slack channels below

#### Slack Channels

| Channel | Purpose |
| ----------- | ----------- |
| [g_engineering_analytics](https://gitlab.slack.com/archives/C01UTSNFS3G) | Channel to discuss team-related work, or for other people to see what's going on with the team & ask any questions related to the team|
| [eng-data-kpi](https://gitlab.slack.com/archives/C0166JCH85U) | Important Announcements related to Engineering Performance Indicators, & forum for external questions to team. |
| [infrafin](https://gitlab.slack.com/archives/C013TFXAVAL) | For questions related specifically GitLab.com hosting cost or other general questions that overlap with infrastructure & finance |
| [development_metrics](https://gitlab.slack.com/archives/C01236LHF1A) | For questions regarding Development Department metrics |
