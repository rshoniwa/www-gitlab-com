---
layout: handbook-page-toc
title: Solutions Architects - Strategic Field Organization
description: "The Strategic Field Organization is a team of Principal Solutions Architects that support global strategic sales opportunities and customers."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
