---
layout: handbook-page-toc
title: "GitLab Project Management Hands On Guide- Lab 2"
description: "This Hands On Guide Lab is designed to walk you through the lab exercises used in the GitLab Project Management course."
---
# GitLab Project Management Hands On Guide- Lab 2
{:.no_toc}

## LAB 2- CREATE A SUB-GROUP AND PROJECT

### Create a Sub-Group 
1. Navigate to **Groups** > **Your Groups** 
2. Navigate to **Training Users** and expand the arrow to the left of Training Users, your training login will be the first one under your class session. 
3. Click the **New Subgroup** button.  
4. In the Group Name field, type ‘*{company-name} New Initiative 1*’  
5. In the Group Description field, you can type any optional description for the sub-group, and then click the **Create Group** button.

### Create a Project
1. Navigate to the **New Project** button** and click it, and then click on **Create Blank Project**.
2. In the Project name dialog box, enter “***[name]PM Hands On***”.  Optionally you may include a few notes in the Project Description Dialog Box. 
3. Under Visibility Level, click the radio button for **Private**.  
4 . Click **Initialize repository with a README** checkbox and then click the green **Create project** button.  
Note: If you do not initialize your repository with a README, you will have a stripped down Git repo, that you will need to create content to bring it into existence.

### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab Project Management- please submit your changes via Merge Request!

