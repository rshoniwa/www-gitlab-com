---
layout: markdown_page
title: "Category Direction - Secrets Management"
description: Our vision is to embed Vault within GitLab and migrate to using it for our own secrets management, within the GitLab Core as well as for your CI Runners. 
canonical_path: "/direction/configure/secrets_management/"
---

- TOC
{:toc}

## Secrets Management

As a result of conflicting interests inside and outside of every organization, everyone has to use and manage secret variables. The vision and philosophy of supporting users with a comprehensive strategy around pipeline permissions, CI/CD variables, deploy keys, and integrations with external key stores are contained within the Secrets Management category at GitLab. 

The first component, [environment variables](https://docs.gitlab.com/ee/ci/variables/), are dynamically-named values that impact the way running processes behave on an operating system. In CI/CD, these values can be secrets that determine how a job executes. Secondarily, [permissions in pipelines](https://docs.gitlab.com/ee/user/project/new_ci_build_permissions_model.html#new-ci-job-permissions-model) are tokens related to how pipelines access other systems inside and outside of GitLab, which is adjacent to the third feature of deploy keys. At GitLab, when [deploy keys](https://docs.gitlab.com/ee/user/project/deploy_keys/) are enabled, it uses an SSH public key to read or read-write to multiple repositories. This is a useful mechanism for sharing access to deploy between projects and automation for remote machines. Lastly, the fourth feature in Secrets Management is around integration with external key stores, of which we are prioritizing HashiCorp Vault first. 

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASecrets%20Management)
- [Overall Vision](/direction/release/)
- [UX Research](https://gitlab.com/gitlab-org/ux-research/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3ASecrets%20Management) 
- [Research Insights](https://gitlab.com/gitlab-org/uxr_insights/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3ASecrets%20Management)

### Secrets Management

Secrets Management is a complex problem space, and GitLab prefers to integrate well with existing secrets management solutions for enterprise needs, and support lighter requirements out of the box.

Our primary integration target for secrets management is HashiCorp's Vault lets you easily rotate secrets and can manage intermediate, temporary tokens used between different services. This ensures there are no long-term tokens lying around or commonly used. Vault minimizes GitLab's attack surface and protects against any unknown zero-day vulnerabilities in our Rails app today or those that allow a bad actor to access the Gitlab server by ensuring that GitLab does not hold any long term secrets. See the [introduction to Vault](https://www.youtube.com/watch?v=pURiZLl6o3w) and our discussion on [Vault<>GitLab Integration](https://www.youtube.com/watch?v=9kD3geEmSJ8) with our CEO @systes.  

While we are not currently shipping Vault as part of Gitlab Omnibus we do desire to create an on-by-default secrets management capability within GitLab. Vault is a strong candidate in this area given our existing integrations, its market penetration, community. As [Vault](https://www.vaultproject.io/) has extensive plugins and [integrations](https://www.vaultproject.io/docs/partnerships#vault-integration-program) we are open to community contributions that extend our Vault integrations.

There are three main secrets management use cases that Vault will solve for. These are as follows: 

- Storing secrets for CI variables and jobs which can be followed in ([gitlab&816](https://gitlab.com/groups/gitlab-org/-/epics/816))
- Managing secrets in general, in own Vault, as illustrated by a collection of features, like a built-in manager for Vault secrets ([gitlab#20306](https://gitlab.com/gitlab-org/gitlab/issues/20306)). 

Check out the 13.0 release of the GitLab <> HashiCorp Vault Integration: 
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Zsdt3Irgjus" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Target audience and experience

Operations, compliance, security, and audit teams will derive immense value from being able to manage secrets within GitLab. In the motion of shifting security left, developers will also be empowered with the comprehensive treatment of variables and keys as a secrets in GitLab. By prioritizing external key store integrations, we will expand GitLab's security by offering an extra layer for tokens, keys, and other confidential data. This combination of tools will further establish GitLab's presence as an enterprise-grade, corporate solution for Release Management. 

## What's next & why

Now that we can effectively authenticate Vault with GitLab from ([gitlab#9983](https://gitlab.com/gitlab-org/gitlab/issues/9983)), and users can install Vault into a Kubernetes clusters via ([gitlab#9982](https://gitlab.com/gitlab-org/gitlab/issues/9982)), we are devoting more time to handling CI Variables from Vault. Users can now authenticate using a JSON Web Token via ([gitlab#207125](https://gitlab.com/gitlab-org/gitlab/issues/207125)), to then fetch and read secrets from Vault via the implementation of ([gitlab#28321](https://gitlab.com/gitlab-org/gitlab/issues/28321)). Simulataneous to this, we are beginning to investigate making pipeline permissions more flexible with a proof of concept in [gitlab#221192](https://gitlab.com/gitlab-org/gitlab/-/issues/221192). Lastly, we are expanding audit logs for CICD variables via [gitlab#30857](https://gitlab.com/gitlab-org/gitlab/-/issues/30857).

## Maturity Plan

This category is currently at the "Viable" maturity level, and our next maturity target is "Complete": (see our [definitions of maturity levels](/direction/maturity/)). We don't have a set plan to around the required developments in order to move this category to "Complete".

## Competitive landscape

The biggest question with respect to Secrets Management integrations is to choose the right partners. The Secrets Management market is a fast moving target with a few, well known providers offering their solutions, and a huge number of users not using any of these. Beside HashiCorp Vault, notable offerings are at least [CyberAkr Conjur](https://www.conjur.org/) and the Secrets Management offering of [AWS](https://docs.aws.amazon.com/secretsmanager/latest/userguide/intro.html), [Google](https://cloud.google.com/secret-manager) and [Azure](https://azure.microsoft.com/en-us/services/key-vault/).

Additionally, [Vault Enterprise](https://www.vaultproject.io/docs/enterprise/) offers
additional sets of capabilities that will _not_ be part of the open source version
of Vault bundled with GitLab. This includes 
[replication across datacenters](https://www.vaultproject.io/docs/enterprise/replication/index.html),
[hardware security modules (HSMs)](https://www.vaultproject.io/docs/enterprise/hsm/index.html),
[seals](https://www.vaultproject.io/docs/enterprise/sealwrap/index.html),
[namespaces](https://www.vaultproject.io/docs/enterprise/namespaces/index.html),
[servicing read-only requests on HA nodes](https://www.vaultproject.io/docs/enterprise/performance-standby/index.html)
(though, the open source version does support [high-availability](https://www.vaultproject.io/docs/concepts/ha.html)),
[enterprise control groups](https://www.vaultproject.io/docs/enterprise/control-groups/index.html),
[multi-factor auth](https://www.vaultproject.io/docs/enterprise/mfa/index.html),
and [sentinel](https://www.vaultproject.io/docs/enterprise/sentinel/index.html).

For customers who want to use GitLab with the enterprise version of Vault, we need
to ensure that this is easy to switch to/use as well.

In the CICD variables space, GitHub [variables](https://docs.github.com/en/actions/configuring-and-managing-workflows/using-environment-variables), offer comparable flexibility and power. They also offer a differentiation between variables and GitHub [secrets](https://docs.github.com/en/actions/configuring-and-managing-workflows/creating-and-storing-encrypted-secrets), which has set an expectation in the market for a distinct treatment of those objects. We are beginning to investigate this for GitLab in [gitlab#217355](https://gitlab.com/gitlab-org/gitlab/-/issues/217355). GitHub Actions supports [injection of HashiCorp Vault Secrets into CICD pipelines](https://www.hashicorp.com/blog/vault-github-action), which is directly competing with GitLab's first-to market offering of HashiCorp Vault [Secrets in GitLab](https://docs.gitlab.com/ee/ci/secrets/).

Deploy keys and tokens are an area where we can complement the GitLab permission paradigm by affording greater flexibility in job permissions. Competition wise, the [GITHUB_TOKEN](https://docs.github.com/en/actions/configuring-and-managing-workflows/authenticating-with-the-github_token), is comparable and more flexible than the CI_JOB_TOKEN today, which we are working to improve in [gitlab&35590](https://gitlab.com/groups/gitlab-org/-/epics/3559).


## Top Customer Success/Sales issue(s)

The top focus for most accounts is to be able to easily integrate with an existing Vault instance. We will render the most value from this after we deliver ([gitlab#28321](https://gitlab.com/gitlab-org/gitlab/-/issues/28321)), which will support the fetching and reading of secrets from Vault for CI jobs. Fit and finish features like managing secrets in the GitLab UI as described in ([gitlab#218677](https://gitlab.com/gitlab-org/gitlab/-/issues/218677)) will reduce the barriers of entry between using Vault and GitLab together.

## Top user issue(s)

The [most popular issues in Secrets Management are listed in GitLab](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3ASecrets+Management&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93).

## Top internal customer issue(s)

The most upvoted `internal customer` issue in Secrets Management is around usability of build variables in [gitlab#17069](https://gitlab.com/gitlab-org/gitlab/-/issues/17069). The second top issue focuses on building out audit logs for CICD variables via [gitlab#30857](https://gitlab.com/gitlab-org/gitlab/-/issues/30857). 

Additionally, our infrastructure and SecOps teams are proceeding to invest in [moving GitLab's own secrets into Vault](https://gitlab.com/groups/gitlab-org/-/epics/1319).

Internally, once the Vault integration is available we can begin moving some of
the secrets tracked internally in GitLab to the included Vault.

- [Examine which secrets we can move to Vault from gitlab.rb](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4297)
- [Discontinue using attr_encrypted](https://gitlab.com/gitlab-org/gitlab/issues/26243)
- [Allow the `db_key_base` secret to be rotated](https://gitlab.com/gitlab-org/gitlab/issues/25332)

## Top Vision Item(s)

Secrets management is a must-have for enterprise-grade release governance. Adding a proper interface to Vault ([gitlab#20306](https://gitlab.com/gitlab-org/gitlab/issues/20306)) embedded in GitLab, would make it easier to interact with the Vault instance. The interface can be leveraged for all secrets, which would also be a competitive feature set for the Operations-centered and security minded buyers within the regulated space. 

We are learning about the desire for there to be a greater differentiation for secrets and CICD variables via [gitlab#217355](https://gitlab.com/gitlab-org/gitlab/-/issues/217355). Users are rightfully expecting secrets to be truly secret to avoid leaking credentials unintentionally. This split will afford a greater fine tuning of variables within GitLab. 

Features to deliver on the compliance and regulatory needs of Secrets Mangement include expansion of Audit events via ([gitlab#8070](https://gitlab.com/gitlab-org/gitlab/-/issues/8070)) and the redefinition of a secret as a separate entity in ([gitlab#217355](https://gitlab.com/gitlab-org/gitlab/-/issues/217355)). Secrets are treated differently than CI/CD variables in practice although GitLab does not distinguish the experience for our users. Investing in this separation and tracking will help our position in [the Zero Trust Wave](https://www.forrester.com/playbook/The+Zero+Trust+Security+Playbook+For+2020/-/E-PLA300) and support our users in their compliance journey.
