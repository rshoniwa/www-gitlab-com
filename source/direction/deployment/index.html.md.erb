---
layout: markdown_page
title: "Deployment Direction"
description: "The job of deploying software is a critical step in DevOps. This page highlights GitLab's direction."
canonical_path: "/direction/deployment/"
---

- TOC
{:toc}

We would appreciate your feedback on this direction page. Please comment on our [async AMA issue](https://gitlab.com/gitlab-com/Product/-/issues/2668), [email Kevin Chu](mailto:kchu@gitlab.com) or [propose an MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/edit/master/source/direction/deployment/index.html.md.erb) to this page!

<%= devops_diagram(["Configure","Release"]) %>

## Overview
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/G-ssIiuwg5E" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### What is Deployment?

Deployment is when you promote a coded, integrated, and built software application to a production environment. Once you deploy an application, users derive value from it. Deployment is part of GitLab's [Ops](/direction/ops/) section.

In the journey to become [elite DevOps performers](https://cloud.google.com/blog/products/devops-sre/using-the-four-keys-to-measure-your-devops-performance), deployment is the hardest challenge. Deployment is where development and operations meet. Deployment is a team sport. It requires empowered developers and it requires guard rails set by operators. In overcoming these hurdles, **teams must**:

* **Enable Developers**: Deployment is simple when done alone. It's hard when done across hundreds of development teams. Doing it well requires automation, orchestration, coordination, and collaboration.
* **Increase Frequency**: Rate-of-iteration is a competitive advantage. Products must deploy frequently to capture value sooner. Increasing deployment frequency compounds the stress on teams responsible for deployments. Teams need fast, repeatable, and safe ways to deploy.
* **Accomodate Target Variety**: Deployment used to involve copying new files to a specific server. Today, deployment targets span environments (dev/staging/production), infrastructure types (VM and container).  Adding to the complexity, these environments and their infrastructure are ephemeral. Operations teams need to provide a consistent interface for deploying to different targets.

Ephemeral infrastructure blurs the line between infrastructure config and software release. As a result, we include our [Configure](/direction/configure/) and [Release](/direction/release/) stages in our Deployment direction. 

### GitLab's Deployment Vision

GitLab's Deployment vision is to enable your organization to be an elite performer in DevOps by making the hardest part of DevOps - deployment - easy, flexible and secure. We will accomplish this because we enable applications to be deployed consistently and frequently in a compliant, reliable, and repeatable way, no matter the deployment target(s) you've defined or how you've organized your applications and teams. We do this by empowering platform engineers to define and automate DevOps practices that have security and compliance guard rails built directly into the process. By empowering the [platform engineer Priyanka](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer), she, in turn, enables developers to own their specific deployment operations without toil, yet retain flexibility and efficiency. Lastly, GitLab deployment takes advantage of the data available in the connected GitLab platform, from planning input to observability and incident data, to make deployment operations, such as scaling rollouts or rollbacks automatic.

### Market
The total addressable market (TAMkt) for DevOps tools targeting the Release and Configure stages was [$1.79B in 2020 and is expected to grow to $3.25B by 2024 (13.8% CAGR) (i)](https://docs.google.com/spreadsheets/d/1LO57cmXHDjE4QRf6NscRilpYA1mlXHeLFwM_tWlzcwI/edit?ts=5ddb7489#gid=1474156035). This analysis is considered conservative as it focuses only on developers and doesn't include other users. External market sizing shows even more potential. For example, continuous delivery alone, which does not include categories such as infrastructure as code, is estimated to have a [market size of $1.62B in 2018 growing to $6B by 2026 (17.76% CAGR)](https://www.verifiedmarketresearch.com/product/continuous-delivery-market/). This, like the [Ops market](/direction/ops/#market) in general, is a deep value pool and represents a significant portion of GitLab's expanding addressable market. 

The deployment tooling market is evolving and expanding. There are now many more options than just adding a delivery job to your CI/CD pipeline. Release Orchestration, advanced deployments, GitOps, infrastructure provisioning, platform as a service, progressive delivery, and feature flags are all methodologies that help teams deploy more easily, frequently, and confidently. Completeness of feature set from vendors is becoming increasingly important as teams want the benefits from all worlds; traditional, table stakes deployment features alongside modern, differentiating features.

To increase deployment frequency and be competitive in the market, enterprises have turned to [centralized cloud teams or cloud center of excellence](https://gitlab.com/gitlab-com/Product/-/issues/2287#note_526578502) that are responsible for helping [development teams be more efficient and productive](https://gitlab.com/gitlab-com/Product/-/issues/2287#note_526579050). These teams have centralized buying power for DevOps and deployment tools. They may also have organizational support to build a DIY DevOps platform. For cloud-native DIY platforms, we've found (through customer interviews) that open-source point deployment solutions (such as [Flux](https://fluxcd.io/) or [ArgoCD](https://argoproj.github.io/argo-cd/)) are the primary options because of their focus on cloud-native principles and early adoption of pull-based deployment. Otherwise, enterprises, even existing GitLab customers, sometimes buy commercial tools for deployment, such as octopus, harness, CloudBees electric flow.
  
### Current Position
The CI/CD pipeline is one of GitLab's most widely used features. Many organizations that have adopted the GitLab pipeline also use GitLab for deployment because it is the natural continuation of their DevOps journey.

Using the CI/CD pipeline, users can create their deployment jobs by writing their own custom `.gitlab-ci.yml` file or using a pre-defined [GitLab Template](https://docs.gitlab.com/ee/ci/examples/README.html#cicd-templates). Users can also leverage GitLab-provided containers to solve specific deployment scenarios, such as [deploying to AWS](https://about.gitlab.com/blog/2020/12/15/deploy-aws/). 

Beyond using the generic pipeline, GitLab offers [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) that enables users to get up and running quickly and easily yet maintain control of each step of the DevOps process.

There are also additional ways to deploy using GitLab. For example, you can [deploy to an attached cluster](https://docs.gitlab.com/ee/user/project/clusters/#deploying-to-a-kubernetes-cluster). You can now also run pull-based GitOps using the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/). Lastly, we are working to make it easier to quickly deploy a stateful app that runs on hypercloud via experimenting through the [5-minute production app](https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template/-/tree/master).

This represents at least 6 deployment-related flavors within GitLab.

1. Users create a custom `.gitlab-ci.yml` for their project.
1. Users use a [templated `.gitlab-ci.yml`](https://about.gitlab.com/blog/2020/09/23/get-started-ci-pipeline-templates/) for their project. Some templates include reference to specific [deploy images](https://about.gitlab.com/blog/2020/12/15/deploy-aws/) to facilitate things working out of the box.
1. Users can use [Auto Devops](https://docs.gitlab.com/ee/topics/autodevops/) to have the pipeline and various GitLab integrations working by default. 
1. Users can attach a [Kubernetes cluster to GitLab](https://docs.gitlab.com/ee/user/project/clusters/). Once done, [those clusters can be deployment targets](https://docs.gitlab.com/ee/user/project/clusters/#deploying-to-a-kubernetes-cluster) with special deployment variables.
1. Users can use the [Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/) and use a GitOps deployment workflow.
1. Users can potentially use [5-minute Production app](https://about.gitlab.com/direction/5-min-production/) to deploy their web apps quickly.

Given the myriad options that exist, we intend to consolidate.

Developers should still be able to create their custom or use templated `.gitlab-ci.yml` for deployments. We plan to make things fast so efforts such as the [5-minute production app](/direction/5-min-production/) are not separate efforts. Platform engineers should be able to define deployment pipeline at the group level, whether using Auto DevOps or not. Users should not attach Kubernetes clusters to GitLab, rather they should use the Kubernetes agent for k8s based deployments and general management. 

Despite the deployment options GitLab provides, we also acknowledge that there are instances where users will choose to integrate with another tool for deployment. If that is the case, we'd love to hear from you to understand why.

## Challenges

The [Ops section challenges](/direction/ops/#challenges) are applicable to deployment. Some challenges are worth expanding on with some additional highlights:

* CNCF/cloud-native open-source solutions can disrupt GitLab's one application vision. They are marketed to a huge and engaged audience. If they're successful in growing adoption, it introduces a barrier to adopting GitLab. For deployment, a risky and highly visible part of the SDLC process, organizations may be more reticent to switch to GitLab's one application solution.
* GitLab's pipeline-based deployment solution targets the developer as the primary persona. As a deployment tool, it may be less effective relative to solutions that target the operator as the primary persona with specific tooling made for their primary jobs.

## Opportunities

The [Ops section opportunities](/direction/ops/#opportunities) apply to deployment. GitLab also has some important opportunities to take advantage of specific to deployment:

* **Deployments builds on GitLab CI/CD:** Deployment is not the same as [CD](https://martinfowler.com/bliki/ContinuousDelivery.html) and won't be solved with the exact same tool as CD. Yet, because GitLab customers already use the CI/CD pipeline as a central component in their software development life cycle, particularly at the project level, building on top of CI/CD is the obvious opportunity as GitLab expands to deployment.
* **DevOps journey is stalled at Deployment:** [Delivering software quickly, reliably, and safely is at the heart of technology transformation and organizational performance, yet still only a small percentage (20%) are elite performers](https://services.google.com/fh/files/misc/state-of-devops-2019.pdf). Small organizations lack the resources to staff a platform team to create efficient deployment capabilities as they scale, large organizations are hindered by the complexity of deployment. Now is the perfect time to enable organizations to get good at deployments through a product.

## Key Capabilities for Deployment

Enterprises are increasingly choosing to have a [cloud-first strategy](https://gitlab.com/gitlab-com/Product/-/issues/2287#note_526573920). Furthermore, with the increasing adoption of microservices architecture and infrastructure as code, traditional release tools are inadequate. This, coupled with the traditional deployment requirement of governance, compliance, and security, means that deployment tools have to meet a high bar and address a set of evolving technology and compliance requirements. The primary themes for these capabilities are that first organizations need **collaboration and transparency**, then **control and compliance** and before requiring **measurement and advanced patterns**. We list these key capabilities of deployment platforms in **priority order of user need**:

**Collaboration and Transparency**
1. **Environment management:** Organizations typically operate multiple environments, each serving different needs. Deployment tools should help to make managing environments easy and intuitive.
1. **Everything as code:** Deployment tooling, including pipelines, infrastructure, environments, and monitoring tools, are constantly evolving. If they can be stored as code and version-controlled, it will enable organizations to more effectively collaborate and avoid costly mistakes.

**Control and Compliance**
1. **GitOps:** Simply checking code into a repository will not prevent drift to occur between code and what is deployed. [GitOps](https://about.gitlab.com/topics/gitops/) solves this problem by automatically managing changes using the single source of truth reflected in the source repository providing more control by preventing drift.
1. **Release Orchestration & Quality gates:** Organizations need to control what can be deployed and in which sequence. Enabling reviews and approvals built right into the deployment workflow is a critical requirement. The ability to perform automatic rollbacks, environment freeze, scaling deployment also enables organizations to be more in control.

**Measurement and Advanced Patterns**
1. **Feedback/Reporting:** Deployment is a critical part of the DevOps feedback loop. A successful deployment depends on both immediate feedback from Monitoring and Observability tools to ensure a healthy deployment. Monitoring the performance of DevOps teams, such as the DORA4 metrics will also lead to improvements over time.
1. **Progressive delivery:** Deployments can be risky. To mitigate risks, progressive delivery techniques, such as using feature flags, can help mitigate the risk by limiting the impact until the deployment teams are confident that their changes are good to go.

## Strategy

In Deployment, we are targeting [Priyanka (Platform Engineer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer) as our primary persona. She is responsible for setting up the systems her company's development teams use to develop, test, **ship**, and **operate** their applications. It is likely she works at an enterprise where there is a mix of deployment targets. She also faces the challenges of ever-increasing complexity; as more contributors, more services, more infrastructure, more databases, and a mix of technologies are added to her applications. She is looking for ways to create a system to manage the complexity.

We plan to iteratively create a solution that addresses priority needs of **collaboration and transparency** before beginning to focus on **control and compliance**. We'll do that by focusing on two pillars. We want to make it easy for Priyanka to:

1. **Enable Developers to deploy to Kubernetes**. Kubernetes is growing fast and also is a shortcut around building specific integrations with cloud vendors. Kubernetes is an efficient target for deployment. It is also where deployment innovation is happening fastest. It is important that we [mitigate this low-end disruption](/direction/#mitigating-low-end-disruption) and make deployments to Kubernetes that work by default. 
1. **Meet Enterprises where they are**. GitLab enterprise customers may strive for progressive delivery features, however we can't expect enterprises to immediately hurdle release challenges that come along with their traditional deployment targets. Their current challenges are with more controlled release processes and the coordination, collaboration and compliance required when orchestrating releases. We want to solve for these initial needs first. This is how we can facilitate the [Golden Journey](https://about.gitlab.com/handbook/product/product-principles/#multi-feature-usage-adoption-journey) from Verify to Release. 

### One Year Plan

To execute our strategy, we are creating two new categories:

1. **Environment Management** - Within GitLab today, Priyanka defines manages environments by defining them directly in `gitlab-ci.yaml`. This works well when she is managing environments per project, but not when she is managing central platform environments that have multiple applications deploying to them from separate projects. She needs visibility to the environments and the deployments to those environments from across multiple projects. 
1. **Platform Management** - Within GitLab today, Priyanka uses `gitlab-ci.yaml` templates and snippets to publish deployment best practices across projects to platforms. This works when the number of projects is small, and the infrastructure and deployment patterns are sufficiently common. This becomes painful when Priyanka is managing complex infrastructure with multiple deployment methods or when trying to understand the specific projects deploying to the platform. Priyanka needs a way to enable multiple teams to follow the best practices she defines across multiple projects. 

And continue our progress on current capabilities:
1. **Kubernetes Management** - We've recently adjusted our approach to connecting Kubernetes clusters to your GitLab instance (with the Kubernetes Agent) and we will add cluster management capabilities to GitLab to be successful here.
1. **Release Orchestration** - We'd previously made investments in improving the experience for more traditional release engineering processes. We are restarting this investment to provide a foundation for platform teams who need to participate in a centrally coordinated release effort. We will shift our focus to enable Priyanka to control access to specific environments, to set up approval workflows. We will also enable Priyanka with tools to set up release managers to organize releases from multiple projects that will be deployed together. 
1. **Infrastructure as Code** - We've made considerable improvements to the way you manage your infrastructure with Terraform from within GitLab. We will continue to build capabilities for sharing, collaborating, and enabling developer use of infrastructure definitions provided by platform teams.
1. **Measure Success** - GitLab is uniquely positioned as a solution to visualize how well teams are shipping software. By surfacing important health metrics, such as the DORA4 metrics, will help fuel improvement and adoption of additional GitLab features.

### What we aren't focused on now
There are important things we won't work on.

1. **Auto DevOps Deployments** - While we will enable the creation of Auto DevOps style templates and experiences for the developers in their organization by platform teams, we will not be making significant strides to make Auto DevOps deployments cover a broader set of use cases at this time.
1. **Progressive Delivery** - By focusing on where platform teams are today, we'll forgo pushing further on our current progressive delivery capabilities like Feature Flags, A/B Testing and Canary Deployments. 
1. **Cost Optimization** - We should first up our Kubernetes Management capability before focusing more on cluster costs. Enterprises want views into costs beyond clusters. Building capabilities like environment management precedes cost optimization tooling.
1. **Non-Kubernetes Cloud-native** - Distinguishing from [Kubernetes-native](https://cloudark.medium.com/towards-a-kubernetes-native-future-3e75d7eb9d42), which is our initial focus area. We will not be focused on other cloud-native solutions, such as Docker Swarm, Apache Mesos, and Amazon ECS, because they're not nearly as successful as Kubernetes.
1. **Building more Deployment Methods** - Actively adding templates or improving existing templates is not our main focus. Nor is building customized images for deploying to cloud providers. The CI/CD pipeline is flexible and enables GitLab users to create what they need as is. It is worthwhile to examine how we can enable the wider GitLab community, including our customer success teams, to share and reuse similar templates and treat them as lego blocks that can be adopted and put to use quickly. These will be most beneficial for common deployment targets, such as FarGate and Lambda.
1. **[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)** - While the deployment of static sites is a large market, GitLab Pages is more of a fit for sharing static webpages within an organization today. We are exploring how to accelerate deploying static content more generally in the [Jamstack SEG](https://about.gitlab.com/handbook/engineering/incubation/jamstack/) 

### What's next

- Release Group - [What's Next for CD](https://about.gitlab.com/direction/release/continuous_delivery/#whats-next--why)
- Configure Group - [What's Next](https://about.gitlab.com/direction/configure/#opportunities)

Beyond Release and Configure, Monitor is also an important aspect of operating your production deployments. We will be adding direction content on this topic.

Lastly, within GitLab, the nomenclature for deployments, environments, release, delivery are often inconsistent, interchangeable, and confusing. We need to settle on a standard and build it into our products and documentation.

## Jobs To Be done

* When deploying my application, I want to orchestrate the entire process, so that the users can use a well-functioning application.
* When operating the development platform, I empower engineers while retaining control with SSO, RBAC, Audit trails, secret management, progressive delivery, and auto-scaling deployments and rollbacks.
* When a developer is using the development platform, I want the developer to be more productive by not having to spend time figuring out how to deploy yet enable them to comply with security and compliance requirements.
* When operating the development platform, I do not want to slow down the development teams and limit what they need to do to improve and deploy their service so that there's no downside to using the platform.
* When changing something in my organization's DevOps process, I do not want to create extra work for my development teams yet enable them to still benefit from the improvements in the process.
* When managing my environments, I want to see and understand what is currently running, so that I can make decisions on what I need to do.
* When deploying my application, it doesn't matter if I am deploying to legacy servers to Kubernetes, it all just works.
* When looking for improvements, I can understand how my organization is performing, so that I can pinpoint actionable improvement areas.

## Competitive Landscape

<%= partial("direction/ops/competition/github-actions") %>

<%= partial("direction/ops/competition/harness") %>

<%= partial("direction/ops/competition/spinnaker") %>

<%= partial("direction/ops/competition/waypoint") %>

<%= partial("direction/ops/competition/argo-cd") %>

<%= partial("direction/ops/competition/weaveworks") %>

<%= partial("direction/ops/competition/jenkins-x") %>

<%= partial("direction/ops/competition/octopus-deploy") %>

<%= partial("direction/ops/competition/cloudbees-electric-flow") %>

<%= partial("direction/ops/competition/IBM-urbancode") %>

<%= partial("direction/ops/competition/digitalai") %>
