title: What an open DevOps platform team can do for your organization
description: "If your DevOps effort is overwhelmed by infrastructure support
  needs, it’s time to consider a cutting-edge addition: a DevOps platform team."
header_body: If your DevOps effort is overwhelmed by infrastructure support
  needs, it’s time to consider a cutting-edge addition - an open DevOps platform
  team.
canonical_path: /topics/devops/how-and-why-to-create-devops-platform-team/
file_name: how-and-why-to-create-DevOps-platform-team
parent_topic: devops
twitter_image: /images/opengraph/gitlab-blog-cover.png
body: >-
  Adopting an open DevOps platform won’t just improve release times – it also
  provides an opportunity to rethink traditional roles, particularly on the ops
  side. Our 2020 Global DevSecOps Survey shows that all DevOps roles are
  changing, but that was especially true in operations. Ops pros told us they
  were taking on new responsibilities including cloud and infrastructure
  management, while devs were busy provisioning and maintaining their own
  environments. Some organizations are going further and creating a DevOps
  platform team to help with the unique challenges of advanced DevOps.


  Here’s a look at why, and how this cutting-edge team could fit into a [DevOps](/topics/devops/) organization.


  ## Start with the open DevOps platform


  Less is certainly more when it comes to an open [DevOps platform](/solutions/devops-platform/); it brings all the steps necessary to build, release, and secure software [together in one place](https://page.gitlab.com/resources-report-gartner-market-guide-vsdp.html) and ends the toolchain “tax.” The platform can serve up advanced technologies from Kubernetes to microservices and infrastructure as code (IaC), and as such, it needs an owner. In the past, a site reliability engineer (SRE) might have been tasked with some of those responsibilities, but today some organizations are looking to hire [DevOps platform engineers](https://about.gitlab.com/topics/devops/what-is-a-devops-platform-engineer/) in order to create a DevOps platform team.


  Not every company with an open DevOps platform will need a team, however. An organization without legacy systems might not need this level of focus on infrastructure, while one with both cloud and on-premises data centers will likely want extra help supporting all the moving parts.


  If you’re not sure, ask this [simple question](https://www.techrepublic.com/article/top-5-things-to-know-about-platform-engineering/): Do you have a system for hosting websites? If the answer is no, a DevOps platform team might be the right choice for you.


  ## What a DevOps platform team can do


  At its heart, a DevOps platform team will free ops (and devs for that matter) from the complex and sometimes messy struggle to support the infrastructure. The goal is, of course, to offer as much self-service as possible for dev and ops, which means a streamlined and less touchy experience. A DevOps platform team can “tame the beast,” making it possible for devs to do push-button deployments without any extra involvement.


  A DevOps platform team will likely take full advantage of infrastructure as code so manual interventions aren’t required. Devs will benefit from an API-interface that will allow them to do their jobs without actually having to understand how the infrastructure is created.


  For some organizations, a DevOps platform team is a way to [maximize engineering efficiency](https://hackernoon.com/how-to-build-a-platform-team-now-the-secrets-to-successul-engineering-8a9b6a4d2c8), and for others it allows for a focus on best practices, an end to ad-hoc platform “volunteer managers” who won’t have a broad view of team goals, and an increase in business agility.


  ## It’s not platform engineer vs. DevOps


  A platform engineering team is an extension of a DevOps team, not a replacement for it. Some practitioners [warn of the risks](https://shahadarsh.com/2017/08/01/enabling-devops-culture-with-a-platform-engineering-team/) of accidentally creating a secondary DevOps team while trying to create a platform team.


  Also, it’s important to keep in mind that a platform engineering team needs a broad set of skills ranging from security to Linux to Kubernetes and of course soft skills like communication and collaboration. A DevOps platform team should be laser focused on infrastructure, not product development.


  Done correctly, a highly-skilled DevOps platform team will [allow the DevOps team to be more efficient](https://dzone.com/articles/the-best-team-structures-for-devops-success).
resources_title: Read more about DevOps
resources:
  - title: The Benefits of DevOps
    url: https://www.youtube.com/watch?v=MNxkyLrA5Aw&feature=emb_logo
    type: Video
  - url: https://about.gitlab.com/customers/axway-devops/
    title: Axway aims for elite DevOps status
    type: Case studies
suggested_content:
  - url: /blog/2020/10/30/future-proof-your-developer-career/
  - url: /blog/2019/10/07/auto-devops-explained/
  - url: /blog/2018/01/22/a-beginners-guide-to-continuous-integration
