title: How SVA enhanced business agility workflow
cover_image: '/images/blogimages/sva.jpg'
cover_title: |
  How SVA enhanced business agility workflow with GitLab
cover_description: |
  SVA uses GitLab for collaboration, version control, test automation, and CI/CD.
twitter_image: '/images/blogimages/sva.jpg'

twitter_text: 'SVA uses GitLab for collaboration, version control, test automation, and CI/CD.'

customer_logo: '/images/case_study_logos/sva-logo.png'
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: Wiesbaden, Germany
customer_solution: GitLab Community Edition
customer_employees: 1,400 employees
customer_overview: |
  SVA’s application and infrastructure development teams adopted GitLab for internal and external SCM, DevOps, and CI/CD.
customer_challenge: |
  SVA was looking for a platform to improve central code management and workflow CI.

key_benefits:
  - |
    Kubernetes integration
  - |
    Single source of truth
  - |
    Jira integration
  - |
    Improved SCM
  - |
    Test automation
  - |
    Cross-group collaboration
  - |
    Markdown and AsciiDoc documentation

customer_stats:
  - stat: 510
    label: Users
  - stat: 110 
    label: Groups
  - stat: 1,571
    label: Projects


customer_study_content:
  - title: the customer
    subtitle: Systems integration leader
    content:
      - |
        System Vertrieb Alexander GmbH (SVA) is a leading systems integration company founded in 1997. Based in Germany, [SVA](https://www.sva.de/index.html) provides a combination of quality IT products with consultation know-how for custom solutions for all industries. The company offers expertise in data center infrastructure, IT platforms, virtualization technologies, software development, applications in general and business intelligence.
      
  - title: the challenge
    subtitle: Improving CI and central code management
    content:
      - |
        SVA has different business-driven lines, all the vendors and services are divided into these nine distinct areas. The focus of one department focus is Agile IT and software development, which includes DevOps and CI/CD. 
      - |
        The code development teams for applications and infrastructure were looking for a solution that could improve its CI workflow and also organize and manage code. SVA was looking for a quickly accessible tool to raise collaboration and code quality for their system engineers. 

  - blockquote: GitLab is easy to use. Our system engineers onboarded very fast. You don’t have to explain every feature. It is very intuitive.
    attribution: Stefan Gärtner
    attribution_title: Head of Competence Center CICD

  - title: the solution
    subtitle: Collaboration, transparency, open source community
    content:
      - |
        SVA uses Jira for issues and Kanban boards for workflow and both continue to be extremely useful. The team reviewed different products for this challenge. After some analysis, the team found that GitLab is the best option for their particular use case. Jira and GitLab integrate easily, so SVA adopted GitLab’s Community Edition for the field engineers.
      - |
        Several SVA system engineers had previous experience using GitLab and were familiar with its features. Many SVA customers have also used GitLab and the engineers now implement it into customer environments. Now, the teams also use GitLab to [deploy Kubernetes](/solutions/kubernetes/) clusters.
      - |
        “If one team wants to look at what the other team is doing or wants to reuse some Ansible playbooks or some code snippets or anything, this is the central point of collecting these things. It's also where they can develop and improve code,” said Sarah Mueck, Head of Business Line Agile IT and Software Development. 

  - title: the results
    subtitle: Internal and external knowledge
    content:
      - |
        SVA has over 500 active GitLab users. Every engineer is now connected to one another in an open source environment. The teams use GitLab as a version control system and also for test automation. “We try to make everything public or most things public. We try to connect every system engineer to each other, like an open source community”, Gärtner said.
      - |
        System engineers use [GitLab CI](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/) for most of their reports and projects. On top of that, GitLab is used in areas that the team hadn’t expected. “We don't use it only as a version control system. I think it's a benefit of GitLab that we are doing test automation now more than before,” Gärtner added.
      - |
        As a consultant company, SVA needs to be able to adapt to the customer environment. By adopting GitLab internally, consultants now have a broader understanding of how their customers work. “That's why we also adopted GitLab internally. Not just for the purpose of what it's used for, but also as a learning curve for our employees that they use it and know how to use it. So if they come to a customer who's using this as well, then they know what they’re doing,” Mueck added.
      - |
        GitLab mirrors the technical company structure. However teams work together within specific projects or repositories. Cross-group work can happen easily within the tool because of the level of transparency. Collaboration happens easier now with one centralized tool in place. 
