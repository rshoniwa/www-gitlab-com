- name: Average age of open vulnerabilities by severity
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The average age of open vulnerabilities gives us a current-state snapshot of how fast we are scheduling and fixing the vulnerabilities found post-Production deploy. This is important because it can help indicate what our future MTTR will look like and whether we are meeting our defined SLOs. The average age is measured in days, and the targets for each severity are defined in the <a href="https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Security Handbook</a>. For Security purposes, please view this chart directly in Sisense.
  target: <a href="https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Time to remediate</a>
  urls:
    - https://app.periscopedata.com/app/gitlab/641782/Appsec-KPIs?widget=8715519&udv=0

  org: Security Department 
  is_key: true
  health:
    level: 1
    reasons:
    - Currently, severity 2 and 3 vulnerabilities are trending outside of our targetted SLOs.

- name: Security Engineer On-Call Page Volume
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: This metric is focused around the volume and severity of paged incidents
    to the Security Engineer On-Call. For Security purposes, please view this chart directly in Sisense.
  target: Number of pages/month does not exceed +50% of monthly average of the last 12 months for 3 consecutive months
  org: Security Department
  is_key: true
  health:
    level: 3
    reasons:
    - Expected fluctuations. 
  urls:
  - https://app.periscopedata.com/app/gitlab/592612/Security-KPIs?widget=9217413&udv=0

- name: Security Control Health
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The objective of this KPI is to minimize exposure to unexpected security risks through continuous control testing. GCF security controls are conitnuously tested as parts of the Compliance team's continuous monitoring program, internal audits and external audits. A clear indicator of program health is directly reflected in the control health effectveness rating (CHER). Observations are a result of GCF security failure, indicating that the control is not implemented, designed effectively or is not operating effectively. These observations indicate a gap that requires remediation in order for the security control to be operating and audit ready.
  target: 60% of executed control tests with a CHER of 1 (Fully Effective)
  org: Security Department
  is_key: true
  health:
    level: 3
    reasons:
    - ~485 control tests have been completed so far this fiscal year against 14 systems. To date over 66% of the controls that have been assessed for these systems have been determined to be fully effective and over 36% not applicable; these not applicable controls are frequently the result of deployment efficiencies.
  sisense_data:
    chart: 11439196
    dashboard: 847984
    embed: v2

- name: Security Impact on Net ARR
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The Field Security organization functions as a sales and customer enablement
    team therefore a clear indicator of success is directly reflected in the engagement
    of their services by Legal, Sales, TAMs and customers themselves.
  target: $4M net ARR per month
  org: Security Department
  is_key: true
  health:
    level: 3
    reasons:
    - Field Security have observed a relative recent decrease on the number of requests, however we have seen a sharp increase in Field Security engagement on high ARR requests.  This aligns with the overall goal of the team to align with the strategic sales goals and allows us to support the larger opportunities.
  urls:
    - https://app.periscopedata.com/app/gitlab/775136/Risk-and-Field-Security-KPIs?widget=11420584&udv=0
  sisense_data:
    dashboard: 775136
    chart: 11420584
    embed: v2 

- name: Cost of Abuse
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: This metric is focused around the financial impact of abusive accounts and their activity. For Security purposes, please view this chart directly in Sisense.
  target: Cost of abuse is below $10K/Mo
  org: Security Department
  is_key: true
  health:
    level: 2
    reasons:
    - The cost of abuse dashboards is an ongoing effort and as more granular usage data sources become available they will be added to create a more holistic view of the costs related to abusive activity on GitLab.com. We are working closely with the Infrastructure department to get additional insights that could help us improve the accuracy of the data presented here.
  urls:
  - https://app.periscopedata.com/app/gitlab/780726/WIP:-Blocked-User-Usage-Data

- name: Security Average Location Factor
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor by function and department so managers can make tradeoffs
    and hire in an expensive region when they really need specific talent unavailable
    elsewhere, and offset it with great people who happen to be in low cost areas.
    Data comes from BambooHR and is the average location factor of all team members
    in the Security department.
  target: Less than 0.70
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - Security Operations having challenges pulling quality candidates in geo-diverse
      locations, in order to hit expectations and allow the team to grow, we will
      revisit our backlog US-based candidates.
  sisense_data:
    chart: 7864119
    dashboard: 592612
    embed: v2
  urls:
  - "/handbook/hiring/charts/security-department/"

- name: Security Budget Plan vs Actuals
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-budget-vs-plan"
  definition: We need to spend our investors' money wisely. We also need to run a
    responsible business to be successful, and to one day go on the public market. For Security purposes, please view this chart directly in Sisense.
  target: Unknown until FY22 planning process
  org: Security Department
  is_key: false
  health:
    level: 1
    reasons:
    - data now lives in adaptive for FY22, not in data warehouse. This is a Q2 priority for data team
  urls:
  - https://app.periscopedata.com/app/gitlab/633239/Security-Non-Headcount-BvAs
- name: Security Handbook MR Rate
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-mr-rate"
  definition: The handbook is essential to working remote successfully, to keeping up our transparency, and to recruiting successfully. Our processes are constantly evolving and we need a way to make sure the handbook is being updated at a regular cadence. This data is retrieved by querying the API with a python script for merge requests that have files matching `/source/handbook/engineering/security` over time.
  target: 1
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - Security highly focused on operations in FY22 Q2, we expect to see a marked increase in handbook updates once project work picks back up.
  sisense_data:
    chart: 10642322
    dashboard: 621064
    shared_dashboard: feac7198-86db-480b-9eae-c41cb479a209
    embed: v2

- name: Security Hiring Actual vs Plan
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-hiring-actual-vs-plan"
  definition: Employees are in the division "Engineering" and department is "Security".
  target: 0.90
  org: Security Department
  is_key: true
  health:
    level: 3
    reasons:
    -  This metric is key until we can hire regularly with quality + velocity again.
  sisense_data:
    chart: 8636794
    dashboard: 592612
    embed: v2
  urls:
  - "/handbook/hiring/charts/security-department/"
- name: Security MR Rate
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-mr-rate"
  definition: This PI is in support of the engineering organization’s overall MR Rate objective however, this should not be considered a reflection of the performance or output of the Security Department whose work is primarily handbook driven.
    Thus, there is no current target for Security Department MR Rate.
    The full definition of MR Rate is linked in the url section.
  target: 0
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons: 
    - There is no current target for Security Department MR Rate.
  urls:
    - "/handbook/engineering/metrics/#merge-request-rate"
    - https://gitlab.com/gitlab-data/analytics/-/issues/4446
  sisense_data:
    chart: 8934521
    dashboard: 686930
    shared_dashboard: ec910110-91bd-4a08-84aa-223bf6b3c772
    embed: v2

- name: Security New Hire Average Location Factor
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor for team members hired within the past 3 months so
    hiring managers can make tradeoffs and hire in an expensive region when they really
    need specific talent unavailable elsewhere, and offset it with great people who
    happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: Less than 0.70
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - We've been fluctuating  above and below the target recently, which is to be expected
      given the size of the department.
  sisense_data:
    chart: 9389232
    dashboard: 719541
    embed: v2
- name: Security Team Member Retention
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-team-member-retention"
  definition: We need to be able to retain talented team members. Retention measures our ability to keep them sticking around at GitLab. Team Member Retention = (1-(Number of Team Members leaving GitLab/Average of the 12 month Total Team Member Headcount)) x 100. GitLab measures team member retention over a rolling 12 month period.
  target: at or above 84%
  org: Security Department
  is_key: true
  public: false
  health:
    level: 2
    reasons:
    - Have been below target for a while
  urls:
    - "https://app.periscopedata.com/app/gitlab/862338/Security-Department-Retention"
- name: Security Vacancy Time to Fill
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-vacancy-time-to-fill"
  definition: Vacancy Time to Fill measures the average time job openings take from open to close. This metric includes sourcing time of candidates compared to Time to Hire or Time to Offer Accept which only measures the time from when a candidate applies to when they accept.
  target: at or below 50 days
  org: Security Department
  is_key: true
  public: true
  health:
    level: 1
    reasons:
    - High demand for Security professionals has lead to an extremely competitive hiring market
  sisense_data:
      chart: 11885848
      dashboard: 872394
      embed: v2
  filters:
      - name: Division
        value: Engineering
      - name: Department
        value: Security
- name: Security Department Discretionary Bonus Rate
  parent: "/handbook/engineering/performance-indicators/#engineering-discretionary-bonus-rate"
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The number of discretionary bonuses given divided by the total number of team members, in a given period as defined. This metric definition is taken from the <a href="/handbook/people-group/people-success-performance-indicators/#discretionary-bonuses">People Success Discretionary Bonuses KPI</a>.
  target: at or above 10%
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
      - Metric is new and is being monitored
  sisense_data:
    chart: 11860249
    dashboard: 873088
    embed: v2
    filters:
      - name: Breakout
        value: Department
      - name: Breakout_Division_Department
        value: Engineering - Security
- name: Security Incidents by Category
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: This metric groups security incidents by incident category. For Security purposes, please view this chart directly in Sisense.
  target: Number of security incidents in any category does not exceed +50% of the individual category's 3-month average.
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - Category labels are applied in accordance to the [SIRT handbook page](https://about.gitlab.com/handbook/engineering/security/security-operations/sirt/). In the event that "NotApplicable" keeps growing, we'll be adding additional categories.
  urls:
  - https://app.periscopedata.com/app/gitlab/592612/Security-KPIs?widget=9277541&udv=0
- name: Security Incidents by Impacted Organization
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: This metric groups security incidents by the impacted organization, service, or part of the business. For Security purposes, please view this chart directly in Sisense.
  target: Number of security incidents in any organization does not exceed +50% of the individual organization's 3-month average.
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - Organization labels are applied in accordance to the [SIRT handbook page](https://about.gitlab.com/handbook/engineering/security/security-operations/sirt/).
  urls:
  - https://app.periscopedata.com/app/gitlab/712011/WIP:-SIRT-Scratchpad?widget=9300071&udv=0

- name: Secure Software Dependency Program Maturity
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: Due to the inherent risk of running 3rd-party code and the current focus on GitLab’s dependency management, this maturity model was created to guide and measure the progress. Over the last 4 quarters work has been done to strengthen the foundation for a world-class dependency management system, reduce the risk associated with software supply chain attacks, and create a market differentiating feature set for GitLab. A lower level must be achieved before the next level up can be achieved, however work and planning preparing for the next level up can occur in parallel. Level 1 - Basic Risk Detection and Manual Processes. Level 2 - Advanced Risk Detection and Manual Processes. Level 3 - Automated Enforcement and Risk Mitigation. For Security purposes, please view this chart with the link provided.
  target: Achieve Level 2 maturity in main GitLab repo by end of FY22Q3
  urls:
    - https://docs.google.com/presentation/d/190CCrlllQqi9LrvcnwVKSvPQ21nBy6QuRVTJKrFpgSs/edit?usp=sharing
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - Currently, we are at 75% completion for Level 2 and on track for 100% completion of Level 2 by the end of FY22-Q3.

- name: Operational Security Risk Management (Tier 2 Risks)
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: Operational risk management enables organizations to proactively identify and mitigate operational security risks that may impact Organizational Output, Brand Reputation, Business Continuity, Customers & Stakeholders, Legal & Regulatory and/or Financials. This heatmap has been generated from ZenGRC. Numbers within each box indicate the total number of potential risks. Red boxes indicate the risk level is HIGH. Orange boxes indicate the risk level is MODERATE. Green boxes indicate the risk level is LOW. The heatmap shows risks that are currently open and accepted, in remediation or planned for remediation.
  target: TBD, this will be determined upon Sisnse integration for detailed dashboarding
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - In FY22Q2 we added an additional 19 net-new risks to this heatmap as an output of the annual security operational risk assessment. Additionally, we noted a total of 7 closed risks and  6 mitigated risks (reduced risks through remediation activities). These changes have resulted in an overall risk posture of Moderate, which is consistent to the risk posture in the prior quarter. We are actively working with Risk Owners to implement risk treatment plans to ensure required acceptances and remediation activities are tracked and documented. 
  sisense_data:
    chart: 11437860
    dashboard: 847984
    embed: v2

- name: Security Observations (Tier 3 Risks)
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: An indicator of information system and process risk, there are multiple inputs that lead to identification of Observations to include Security Compliance continuous control testing, Internal Audit control testing, third party (vendor) assessments, external audits and customer assessments. 
  target: TBD, this will be determined upon Sisnse integration for detailed dashboarding
  org: Security Department
  is_key: false
  health:
    level: 1
    reasons:
    - There are currently 51 high risk observations open which represents 38% of all open observations. 12 of these high risk observations have been opened since the beginning of Q2 with the majority of the observations resulting from Q1 security control testing. 7 high-risk observations have been closed to date this fiscal year. We are actively working with the control owners toward remediation. 
  sisense_data:
    chart: 11426201
    dashboard: 847984
    embed: v2

- name: Third Party Risk Management
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: An indicator of third party risk, third party risk assessments proactively identify potential vendor security risks as part of onboarding or contracting, enabling business owners to make risk based decisions throughout the vendor lifecycle.  
  target: TBD, this will be determined upon Sisense integration for detailed dashboarding
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - The TPRM program expects a continued average increase of 10% net new requests quarter over quarter. We began FY22 Q2 with 30 open requests and to date have received an additional 81 requests this quarter. Of those, approximately 60% were Low Inherent Risk. The remaining vendors underwent a full security assessment which resulted in the identification of 2 Moderate Risk Vendors and 3 High Risk Vendors. Remediation plans are underway for these 5 vendors.
  sisense_data:
    chart: 11439229
    dashboard: 847984
    embed: v2

- name: Security Automation OKR/EPIC Completion
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: All Security Automation OKRs should reach 80% or higher by the end of any active quarter. This measurement indicates how well the Security Automation team is executing on the highest priorities of the quarter which are aligned with business needs and goals.
  target: 80
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons: 
    - The Security Automation team is still tweaking their agile processes.
  sisense_data:
    chart: 11478764
    dashboard: 850740
    shared_dashboard: a9d1978c-e247-4299-a5e2-a34934c06feb
    embed: v2

- name: Security Automation Iteration Velocity
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: We attempt to complete 7 issues or more every two weeks. The measurement indicates how well the Security Automation team is scoping milestones and provides a view of the team velocity over the last 10 milestones. While our average iteration is 2 weeks in length, in an effor to maintain flexibility, we allow for 1 week and 3 week long iterations as well, thus the data in the chat is measured by 1 week long increments. 
  target: 7
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons: 
    - The security automation team is still tweaking their agile processes.
  sisense_data:
    chart: 11480449
    dashboard: 850740
    shared_dashboard: a9d1978c-e247-4299-a5e2-a34934c06feb
    embed: v2

- name: Security Automation Iteration Velocity Average
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: We attempt to complete 7 issues or more every two weeks. The measurement indicates how well the Security Automation team is scoping milestones over the last 10 iterations and provides a view of the average team velocity.
  target: 7
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons: 
    - The security automation team is still tweaking their agile processes.
  sisense_data:
    chart: 11480503
    dashboard: 850740
    shared_dashboard: a9d1978c-e247-4299-a5e2-a34934c06feb
    embed: v2

- name: Top 3 oldest open vulnerabilities by severity
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The top 3 oldest open vulnerabilities by severity metric gives us a highly actionable view and direction of which vulnerabilities need the most attention and which to focus on fixing first.
  target: Ages are within SLOs <a href="https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Time to remediate</a>
  urls:
    - https://app.periscopedata.com/app/gitlab/839979/Appsec-Sandbox?widget=11492207&udv=0
  org: Security Department
  is_key: false
  health:
    level: 1
    reasons:
    - The age of these vulnerabilities are significantly beyond our target SLOs.

